use axum::{http::StatusCode, routing::get, Extension, Json, Router};
use serde::{Deserialize, Serialize};
use sqlx::{mysql::MySqlPoolOptions, MySql, Pool};
use std::net::SocketAddr;
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Deserialize, Serialize)]
struct Reading {
    room_name: String,
    temperature: f64,
    humidity: f64,
    air_pressure: f64,
    voc: f64,
}

#[tokio::main]
async fn main() {
    let database_url = "mysql://envcollector:envcollector@localhost:3306/envcollector";
    let db_pool = MySqlPoolOptions::new()
        .max_connections(2)
        .connect(database_url)
        .await
        .unwrap();

    let app = Router::new()
        .route("/", get(handler))
        .layer(Extension(db_pool));

    let listen_address = SocketAddr::from(([0, 0, 0, 0], 8080));

    axum::Server::bind(&listen_address)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn handler(
    Extension(db_pool): Extension<Pool<MySql>>,
    Json(reading): Json<Reading>,
) -> StatusCode {
    let timestamp = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("valid timestamp")
        .as_millis();
    match sqlx::query("INSERT INTO reading VALUES (0, ?, ?, ?, ?, ?, ?)")
        .bind(reading.room_name)
        .bind(reading.temperature)
        .bind(reading.humidity)
        .bind(reading.air_pressure)
        .bind(reading.voc)
        .bind(timestamp as u64)
        .execute(&db_pool)
        .await
    {
        Ok(_) => StatusCode::OK,
        Err(_) => StatusCode::INTERNAL_SERVER_ERROR,
    }
}
