# code kata - REST/CRUD webservice

A generic REST webservice that persists data in a database. Intended as small project to try out different languages, frameworks and approaches.

The real life purpose is to collect environment sensor readings (e.g. temperature) from different rooms, persist this and make it available for an UI.

As everything will be running in a local network only, it does not need security at this point (also to simplify things), though that might be added later.

## kata purpose

As previously mentioned, the main purpose is to try out different programming languages and/or frameworks for educational and fun reasons.
Ideally, the kata also serves for statistics and self-reflection:
- How long did it take to do the task in x?
- How productive did it feel?
- How was the development experience?
- What tools can be used for this?
- Can this be used for my day-to-day projects at #bigcorp?

## requirements for the webservice

- provide JSON/REST endpoints
- provide endpoint for creation of new entries (e.g. `POST /reading`), with the following fixed data points:
  - room name (string, fixed for each room, should be base64 encoded)
  - temperature (floating point number)
  - humidity (floating point number)
  - air pressure (floating point number)
  - voc (floating point number)
- persist these entries along with a unique id (primary key) and current_timestamp
- use MariaDB (or MySQL) for persistence

strictly optional:
- provide endpoints to read entries:
  - for all rooms (e.g. `GET /reading/rooms`)
  - for a specific room (e.g. `GET /reading/rooms/dGVzdA==`)
  - for a specific time window (e.g. `GET /reading/rooms?start_date=123&end_date=987`)
- provide endpoint to update specific entries by id (e.g. `PUT /reading`)
- provide endpoint to delete specific entries by id (e.g. `DELETE /reading/123`)


## planned implementations

|lang      |framework                     |done|
|----------|------------------------------|----|
|Java 17   |Spring boot, web framework    |    |
|Java 17   |Spring boot, reactor framework|    |
|Java 17   |Quarkus / vert.x              |    |
|rust      |actix, warp                   |    |
|rust      |axum                          |(x) |
|go        |standard library?             |    |
|nodejs    |standard library?             |    |

As you might have guessed, I'm mostly a Java developer, trying to explore other stuff :)


# Implementations

## java14-springboot

Done already, but so long ago that I don't remember many details. Probably at the time when Java 14 was new :-)

## rust-warp

Also done a long time ago. This is the version that is currently running and collecting the data. Will be replaced by another version soon.

## rust-axum

Following the examples of axum and sqlx, it was rather straight forward to implement a very simple application implementing the minimal requirements. Duration was roughly 2 hours, I should have used a stopwatch.


