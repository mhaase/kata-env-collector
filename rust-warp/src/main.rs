use mysql_async::prelude::*;
use serde::{Deserialize, Serialize};
use std::convert::Infallible;
use std::time::{SystemTime, UNIX_EPOCH};
use warp::{http, Filter, Rejection, Reply};

#[derive(Deserialize, Serialize)]
struct Reading {
    room_name: String,
    temperature: f64,
    humidity: f64,
    air_pressure: f64,
    voc: f64,
}

#[derive(Deserialize, Serialize)]
struct ReadingEntity {
    id: i64,
    room_name: String,
    temperature: f64,
    humidity: f64,
    air_pressure: f64,
    voc: f64,
    create_date: i64,
}

#[tokio::main]
async fn main() {
    let db_pool = get_db_pool();

    let routes = route_create_entry(db_pool.clone())
        .or(route_get_entries(db_pool.clone()))
        .or(route_get_entries(db_pool.clone()));

    warp::serve(routes).run(([127, 0, 0, 1], 8080)).await
}

fn route_create_entry(
    db_pool: mysql_async::Pool,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::post()
        .and(warp::path("reading"))
        .and(warp::body::content_length_limit(1024 * 16))
        .and(warp::body::json())
        .and(with_db(db_pool.clone()))
        .and_then(|reading: Reading, db_pool: mysql_async::Pool| async move {
            create_entry(db_pool, reading).await
        })
}

fn route_get_entries(
    db_pool: mysql_async::Pool,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::get()
        .and(warp::path("reading"))
        .and(with_db(db_pool.clone()))
        .and_then(|db_pool: mysql_async::Pool| async move { get_entries(db_pool).await })
}

fn with_db(
    db_pool: mysql_async::Pool,
) -> impl Filter<Extract = (mysql_async::Pool,), Error = Infallible> + Clone {
    warp::any().map(move || db_pool.clone())
}
async fn create_entry(
    db_pool: mysql_async::Pool,
    reading: Reading,
) -> Result<impl Reply, Rejection> {
    let mut conn = db_pool.get_conn().await.expect("connection available");
    let params = params! {
        "room_name" => reading.room_name,
        "temperature" => reading.temperature,
        "humidity" => reading.humidity,
        "air_pressure" => reading.air_pressure,
        "voc" => reading.voc,
        "create_date" => SystemTime::now().duration_since(UNIX_EPOCH).expect("valid timestamp").as_millis(),
    };
    conn.exec_drop("INSERT INTO reading (room_name, temperature, humidity, air_pressure, voc, create_date) VALUES (:room_name, :temperature, :humidity, :air_pressure, :voc, :create_date)", params)
    .await
    .expect("insert successful");
    Ok(http::StatusCode::OK)
}

async fn get_entries(db_pool: mysql_async::Pool) -> Result<impl Reply, Infallible> {
    let mut conn = db_pool.get_conn().await.expect("connection available");

    let readings = conn.exec_map(
        "SELECT id, room_name, temperature, humidity, air_pressure, voc, create_date FROM reading",
        (),
        |(id, room_name, temperature, humidity, air_pressure, voc, create_date)| ReadingEntity {
            id,
            room_name,
            temperature,
            humidity,
            air_pressure,
            voc,
            create_date,
        },
    )
    .await
    .expect("successful SELECT of readings");

    Ok(warp::reply::json(&readings))
}

fn get_db_pool() -> mysql_async::Pool {
    let database_url = "mysql://envcollector:envcollector@localhost:3306/envcollector";

    mysql_async::Pool::new(database_url)
}
