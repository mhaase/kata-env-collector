package de.mh.kata.envcollector.entity.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import de.mh.kata.envcollector.entity.ReadingEntity;

public interface ReadingRepository extends JpaRepository<ReadingEntity, Long> {

    List<ReadingEntity> findByRoomName(String roomName);

    List<ReadingEntity> findByCreateDateAfterAndCreateDateBefore(long startDate, long endDate);

    List<ReadingEntity> findByRoomNameAndCreateDateAfterAndCreateDateBefore(String roomName, long startDate,
            long endDate);
}
