package de.mh.kata.envcollector.boundary;

import java.util.List;
import java.util.stream.Collectors;

import javax.websocket.server.PathParam;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.mh.kata.envcollector.boundary.bean.Reading;
import de.mh.kata.envcollector.entity.ReadingEntity;
import de.mh.kata.envcollector.entity.repo.ReadingRepository;

@RestController
@RequestMapping("reading")
public class ReadingController {

    private final ReadingRepository readingRepository;

    public ReadingController(ReadingRepository readingRepository) {
        this.readingRepository = readingRepository;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void storeReading(@RequestBody Reading reading) {
        readingRepository.save(mapReading(reading));
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Reading> getAllReadings(@RequestParam("start_date") Long startDate,
            @RequestParam("end_date") Long endDate) {
        List<ReadingEntity> readings;
        if (startDate != null && endDate != null) {
            readings = readingRepository.findByCreateDateAfterAndCreateDateBefore(startDate, endDate);
        } else {
            readings = readingRepository.findAll();
        }
        return readings.stream().map(this::mapReadingEntity).collect(Collectors.toList());
    }

    @GetMapping(path = "/{roomName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Reading> getReadingsForRoom(@PathParam("roomName") String roomName,
            @RequestParam("start_date") Long startDate, @RequestParam("end_date") Long endDate) {
        List<ReadingEntity> readings;
        if (startDate != null && endDate != null) {
            readings = readingRepository.findByRoomNameAndCreateDateAfterAndCreateDateBefore(roomName, startDate,
                    endDate);
        } else {
            readings = readingRepository.findByRoomName(roomName);
        }
        return readings.stream().map(this::mapReadingEntity).collect(Collectors.toList());
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateReading(Reading reading) {
        // only upsert is provided, so we will check if the reading really exists
        readingRepository.findById(reading.getId()).map(ReadingEntity::getId).ifPresentOrElse(id -> {
            var e = mapReading(reading);
            e.setId(id);
            readingRepository.save(e);
        }, () -> {
            throw new IllegalStateException("not found");
        });
    }

    @DeleteMapping(path = "/{id}")
    public void removeReading(@PathParam("id") long id) {
        readingRepository.deleteById(id);
    }

    private ReadingEntity mapReading(Reading reading) {
        var e = new ReadingEntity();

        e.setRoomName(reading.getRoomName());
        e.setTemperature(reading.getTemperature());
        e.setHumidity(reading.getHumidity());
        e.setAirPressure(reading.getAirPressure());
        e.setVoc(reading.getVoc());

        e.setCreateDate(System.currentTimeMillis());
        return e;
    }

    private Reading mapReadingEntity(ReadingEntity e) {
        var reading = new Reading();
        reading.setId(e.getId());
        reading.setRoomName(e.getRoomName());
        reading.setTemperature(e.getTemperature());
        reading.setHumidity(e.getHumidity());
        reading.setAirPressure(e.getAirPressure());
        reading.setVoc(e.getVoc());
        reading.setCreateDate(e.getCreateDate());
        return reading;
    }
}
