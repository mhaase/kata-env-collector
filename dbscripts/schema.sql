
CREATE DATABASE envcollector;
CREATE USER 'envcollector'@'localhost' IDENTIFIED BY 'envcollector';
GRANT ALL PRIVILEGES ON envcollector.* TO 'envcollector'@'localhost';
FLUSH PRIVILEGES;

USE envcollector

CREATE TABLE reading (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `room_name` VARCHAR(255) NOT NULL,
  `temperature` FLOAT DEFAULT NULL,
  `humidity` FLOAT DEFAULT NULL,
  `air_pressure` FLOAT DEFAULT NULL,
  `voc` FLOAT DEFAULT NULL,
  `create_date` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_room_name` (`room_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;